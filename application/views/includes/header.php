
<!doctype html>
<html lang="en">

<head>

  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Graha Medika</title>
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,400&display=swap" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />  

  <!-- Template CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/style-starter.css">
	<script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>

</head>

<body>
<!-- site header -->
<header id="site-header" class="fixed-top">
  <nav class="navbar navbar-expand-lg navbar-dark">
      <a class="navbar-brand" href="<?php echo base_url(); ?>dashboard">
          <span class=""></span> Graha Medika
      </a>
       <?php if($this->session->userdata('isLoggedIn')){
        
         ?>
       
                
      <button class="navbar-toggler bg-gradient" type="button" data-toggle="collapse"
          data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
          aria-label="Toggle navigation">
          <a href="<?php echo base_url(); ?>logout" class="btn btn-primary btn-style">Sign out</a>
      </button>
                 
                </ul>
              </li>
        <?php }else {?>
          <button class="navbar-toggler bg-gradient" type="button" data-toggle="collapse"
          data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false"
          aria-label="Toggle navigation">
          <a href="<?php echo base_url(); ?>login" class="btn btn-primary btn-style">Login</a>
      </button>

         
              <?php }?>
      

      <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav m-auto">
              <li class="nav-item active"></li>
              <li class="nav-item"></li>
              <li class="nav-item"></li>
              <li class="nav-item"></li>
          </ul>
          <ul class="navbar-nav m-auto">
              <li class="nav-item active"></li>
              <li class="nav-item"></li>
              <li class="nav-item"></li>
              <li class="nav-item"></li>
          </ul>
          <ul class="navbar-nav m-auto">
              <li class="nav-item active"></li>
              <li class="nav-item"></li>
              <li class="nav-item"></li>
              <li class="nav-item"></li>
          </ul>
          <ul class="navbar-nav m-auto">
          <?php if($this->session->userdata('isLoggedIn')){
        
         ?>
       
                  <!-- Menu Footer-->
                    <div class="pull-right">
                      <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                    </div>
      
                 
                </ul>
              </li>
        <?php }else {?>
         <ul class="navbar-nav">
              <li class="nav-item">
                  <a href="<?php echo base_url(); ?>login" class="btn btn-primary btn-style">Login</a>
              </li>
          </ul>

         
              <?php }?>
          </ul>
      </div>
  </nav>
</header>
<!-- //site header -->
