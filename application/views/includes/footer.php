
<!-- site footer -->
<footer id="site-footer">
  <div class="bottom-footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 text-lg-left text-center mb-lg-0 mb-3">
          <p class="copyright">© 2020 Graha Medika. All Rights Reserved. </p>
        </div>
        <div class="col-lg-4 align-center text-lg-right text-center">
        </div>
      </div>
    </div>
  </div>
</footer>
<!-- //site footer -->

<!-- move top -->
<button onclick="topFunction()" id="movetop" class="bg-primary" title="Go to top">
  <span class="fa fa-angle-up"></span>
</button>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "userListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script>
<script>
  // When the user scrolls down 20px from the top of the document, show the button
  window.onscroll = function () {
    scrollFunction()
  };
  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      document.getElementById("movetop").style.display = "block";
    } else {
      document.getElementById("movetop").style.display = "none";
    }
  }
  // When the user clicks on the button, scroll to the top of the document
  function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }
</script>
<!-- //move top -->

<!-- javascript -->
<script src="<?php echo base_url(); ?>assets/assets/js/jquery-3.3.1.min.js"></script>

<!-- libhtbox -->
<script src="<?php echo base_url(); ?>assets/assets/js/lightbox-plus-jquery.min.js"></script>

<!-- particles -->
<script src='<?php echo base_url(); ?>assets/assets/js/particles.min.js'></script>
<script src="<?php echo base_url(); ?>assets/assets/js/script.js"></script>
<!-- //particles -->

<!-- owl carousel -->
<script src="<?php echo base_url(); ?>assets/assets/js/owl.carousel.js"></script>
<script>
  $(document).ready(function () {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
      margin: 10,
      nav: true,
      loop: false,
      responsive: {
        0: {
          items: 1
        },
        767: {
          items: 2
        },
        1000: {
          items: 3
        }
      }
    })
  })
</script>

<!-- disable body scroll which navbar is in active -->
<script>
  $(function () {
    $('.navbar-toggler').click(function () {
      $('body').toggleClass('noscroll');
    })
  });
</script>
<!-- disable body scroll which navbar is in active -->

<!-- bootstrap -->
<script src="<?php echo base_url(); ?>assets/assets/js/bootstrap.min.js"></script>

</body>
</html>
