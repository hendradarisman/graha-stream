
<!-- about page breadcrumns -->
<section class="inner-banner">

</section>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb breadcrumb-padding">
        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Stream</li>
    </ol>
</nav>
<!-- //about page breadcrumns -->

<!-- about page section1-->
<section class="w3l-about">
    <div class="container">
    <h4 class="section-title">Webminar Graha Medika</h4>
</br></br>
        <div class="row about-content">
            
            <div class="col-lg-6 info mb-lg-0 mb-5 align-center">
            <iframe type="text/html" width="1200" height="600" src="https://www.youtube.com/embed/fxhk5OC_wEw?modestbranding=1&rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
            </div>
            
        </div>
    </div>
</section>
<!-- //about page section1-->

<!-- /mobile section --->
<section class="w3l-mobile-content-6">
    
</section>
<!-- //mobile section --->

<!-- about page form section -->
