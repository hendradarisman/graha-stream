<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Excel_import extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('excel_import_model');
		$this->load->library('excel');
		$this->isLoggedIn();
	}

	function index()
	{
		// $this->load->view('excel_import');
		$data['show'] = $this->excel_import_model->show();
		$this->loadViews("excel_import", $this->global, $data , NULL);
	}
	
	function fetch()
	{
		if($this->isAdmin() == TRUE )
        {
            $this->loadThis();
        } else
        {     
		$data = $this->excel_import_model->select();
		$output = '
		<h3 align="center">Total Data - '.$data->num_rows().'</h3>
		<table class="table table-striped table-bordered">
			<tr>
				<th>Username</th>
				<th>Password</th>
			
			</tr>
		';
		foreach($data->result() as $row)
		{
			$output .= '
			<tr>
				<td>'.$row->email.'</td>
				<td>'.getHashedPassword($row->password).'</td>
			
			</tr>
			';
		}
		$output .= '</table>';
		echo $output;
	}
}

	function import()
	{
		if(isset($_FILES["file"]["name"]))
		{
			$path = $_FILES["file"]["tmp_name"];
			$object = PHPExcel_IOFactory::load($path);
			foreach($object->getWorksheetIterator() as $worksheet)
			{
				$highestRow = $worksheet->getHighestRow();
				$highestColumn = $worksheet->getHighestColumn();
				for($row=2; $row<=$highestRow; $row++)
				{
					$username = $worksheet->getCellByColumnAndRow(0, $row)->getValue();
					$password = $worksheet->getCellByColumnAndRow(1, $row)->getValue();
					// $city = $worksheet->getCellByColumnAndRow(2, $row)->getValue();
					// $postal_code = $worksheet->getCellByColumnAndRow(3, $row)->getValue();
					// $country = $worksheet->getCellByColumnAndRow(4, $row)->getValue();
					$data[] = array(
					
						'email'				=>	$username,
						'password'			=>  getHashedPassword($password),
						'roleId'			=>	'3',
						'name'				=>	'Member'
						
					);
				}
			}
			$this->excel_import_model->insert($data);
			echo 'Data Imported successfully';
		}	
	}
	
    public function editLink(){


        $databos = [


            'id' => $this->input->post('id', true),


            'link' => $this->input->post('link', true)


        ];        


        $this->db->where('id', $this->input->post('id', true));


        $this->db->update('tbl_link', $databos);
		redirect('excel_import');

    }

}

?>